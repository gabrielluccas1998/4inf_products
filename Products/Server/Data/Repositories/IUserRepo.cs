﻿using Products.Shared.Models;

namespace Products.Server.Data.Repositories
{
    public interface IUserRepo
    {
        Task<List<User>> GetAllUsersAsync();
        Task DeleteUserByIdAsync(int id);
        Task CreateNewUser(User user);
        Task SaveChangesAsync();
        Task GetUserByIdAsync();
    }
}
