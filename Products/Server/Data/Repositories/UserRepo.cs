﻿using Microsoft.EntityFrameworkCore;
using Products.Shared.Models;

namespace Products.Server.Data.Repositories
{
    public class UserRepo:IUserRepo
    {
        private ProductDbContext _dbContext;

        public UserRepo(ProductDbContext dbContext)
        {
            this._dbContext = dbContext;
        }
        public async Task<List<User>> GetAllUsersAsync()
        {
            return await _dbContext.Users.ToListAsync();
        }

        public async Task<User> GetUserByIdAsync(int id)
        {
            return await _dbContext.Users.FirstOrDefaultAsync(x => x.UserID == id);
        }

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteUserByIdAsync(int id)
        {
            var user = await GetUserByIdAsync(id);
            _dbContext.Users.Remove(user);
        }

        public async Task CreateNewUser(User u)
        {
            _dbContext.Users.Add(u);
            await _dbContext.SaveChangesAsync();
        }
    }
}
