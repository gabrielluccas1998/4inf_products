﻿using Microsoft.EntityFrameworkCore;
using Products.Shared.Models;

namespace Products.Server.Data
{
    public class ProductDbContext:DbContext
    {
        public ProductDbContext(DbContextOptions<ProductDbContext> options):base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<OrderPosition>()
                .HasKey(x => new { x.OrderNr, x.PositionNr });
            modelBuilder.Entity<User>().HasData(
                new User { UserID = 1, FirstName = "Ralph", LastName = "Kartnaller", LoginName = "Ralph", Password = "ralph123" },
                new User { UserID = 2, FirstName = "Max", LastName = "Mustermann", LoginName = "Max", Password = "max123"}    
                );       
        }
        public virtual DbSet<ProductProperty> ProductProperties { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<OrderPosition> OrderPositions { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
    }
}
