﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Products.Server.Migrations
{
    public partial class newusers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "UserID", "Created", "Expires", "FirstName", "LastName", "LoginName", "Password" },
                values: new object[] { 1, new DateTime(2021, 11, 29, 8, 10, 37, 279, DateTimeKind.Local).AddTicks(7667), new DateTime(2021, 12, 29, 8, 10, 37, 279, DateTimeKind.Local).AddTicks(7697), "Ralph", "Kartnaller", "Ralph", "ralph123" });

            migrationBuilder.InsertData(
                table: "User",
                columns: new[] { "UserID", "Created", "Expires", "FirstName", "LastName", "LoginName", "Password" },
                values: new object[] { 2, new DateTime(2021, 11, 29, 8, 10, 37, 279, DateTimeKind.Local).AddTicks(7702), new DateTime(2021, 12, 29, 8, 10, 37, 279, DateTimeKind.Local).AddTicks(7703), "Max", "Mustermann", "Max", "max123" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "UserID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "User",
                keyColumn: "UserID",
                keyValue: 2);
        }
    }
}
