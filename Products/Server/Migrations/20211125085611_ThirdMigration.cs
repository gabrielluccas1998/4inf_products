﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Products.Server.Migrations
{
    public partial class ThirdMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderPosition_Order_OrderNr1",
                table: "OrderPosition");

            migrationBuilder.DropIndex(
                name: "IX_OrderPosition_OrderNr1",
                table: "OrderPosition");

            migrationBuilder.DropColumn(
                name: "OrderNr1",
                table: "OrderPosition");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderPosition_Order_OrderNr",
                table: "OrderPosition",
                column: "OrderNr",
                principalTable: "Order",
                principalColumn: "OrderNr",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderPosition_Order_OrderNr",
                table: "OrderPosition");

            migrationBuilder.AddColumn<int>(
                name: "OrderNr1",
                table: "OrderPosition",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_OrderPosition_OrderNr1",
                table: "OrderPosition",
                column: "OrderNr1");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderPosition_Order_OrderNr1",
                table: "OrderPosition",
                column: "OrderNr1",
                principalTable: "Order",
                principalColumn: "OrderNr",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
