﻿using Microsoft.AspNetCore.Components;
using Products.Shared.Models;

namespace Products.Client.Pages.User_Pages
{
    public partial class EditUser
    {
        [Parameter]
        public string UserId { get; set; }
        public User User { get; set; } = new User();
    }
}